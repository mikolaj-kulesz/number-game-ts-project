import {ProgressBar} from "./components/progress-bar";
import {Events} from "./models/interfaces";
import {Modal} from "./components/modal";
import {GameController} from "./components/game-controller";
import {gameState} from "./state/game-state";
import {Timer} from "./components/timer";
import {LevelCounter} from "./components/levelCounter";
import {MistakeCounter} from "./components/mistakeCounter";
import {Ranking} from "./components/ranking";

class Game {

    private cardsNumber: number = 0
    private setOfNumbers: number[] = []
    private wrapper: HTMLDivElement

    constructor() {
        this.wrapper = this.getWrapper()
        this.init(true)
    }

    private init(firstTime: boolean = false) {
        this.initializeComponents()
        this.initializeState(firstTime)
        this.registerListeners()
    }

    private registerListeners(): void {
        gameState.addListener({
            [Events.START_NEW_LEVEL]: () => {
                this.startNewLevel()
            },
            [Events.RESTART]: () => {
                this.restart()
            }
        })
    }

    private initializeComponents() {
        new GameController()
        new ProgressBar()
        new Timer()
        new LevelCounter()
        new MistakeCounter()
        new Ranking()
        this.initializeModals()
    }

    private initializeModals() {
        [
            Events.WELCOME,
            Events.SUCCESS,
            Events.FAILURE,
            Events.TIMEUP,
            Events.WIN
        ].forEach(customEvent => new Modal(customEvent))
    }

    private getWrapper(): HTMLDivElement {
        return document.querySelector('#app')! as HTMLDivElement
    }

    private initializeState(firstTime: boolean = false) {
        // this.cardsNumber = this.getRandomNumber(3, 10)

        this.setOfNumbers = this.generateRandomSetOfNumbers(gameState.level + 2, 100)

        gameState.minValue = 1
        gameState.maxValue = this.setOfNumbers[this.setOfNumbers.length - 1]
        gameState.currentValue = this.setOfNumbers[0]
        gameState.currentIndex = 0
        gameState.stepValue = 1 / this.setOfNumbers.length
        const aaa = this.shuffleCards(this.setOfNumbers)
        gameState.setState({
            setOfNumbers: this.setOfNumbers,
            shuffledSetOfNumbers: aaa
        })
        if (firstTime) gameState.triggerEvent(Events.WELCOME)
        else gameState.triggerEvent(Events.START)
    }

    private getRandomNumber(min: number, max: number): number {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    private generateRandomSetOfNumbers(length: number, max: number): number[] {
        const aaa = Array(length).fill(0).map(() => Math.round(Math.random() * max)).sort((a, b) => a - b)
        return [...new Set(aaa)];
    }

    private emptyWrapper() {
        this.wrapper.innerHTML = ''
    }

    private shuffleCards(b: number[]) {
        const a = [...b]
        for (let i = a.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [a[i], a[j]] = [a[j], a[i]];
        }
        return a;
    }

    private restart(): void {
        gameState.setState({
            totalPlayTime: 0,
        })
        this.initializeState()
    }

    private startNewLevel(): void {
        this.initializeState()
    }
}

new Game()

