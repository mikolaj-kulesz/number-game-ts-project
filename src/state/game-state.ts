import {Events, ListenerCollection, ListenerObject} from "../models/interfaces"

class State<T> {
    private eventListeners: ListenerCollection = {}
    private debug: boolean = true
    public addListener(listener: ListenerObject): void {
        Object.keys(listener).forEach(key => {
            if (key in Events) this.createListener(key as Events, listener)
        })
    }

    public run(e: Events, identifier?: number) {
        if(this.debug) console.log(e);
        if(!this.eventListeners[e]) {
            throw new Error(`No event listeners for: ${e}`);
        }
        this.eventListeners[e].forEach((listener) => {
            if (identifier || identifier === 0) {
                listener(identifier)
                return
            }
            listener()
        })
    }

    private createListener(key: Events, listener: ListenerObject): void {
        if (!this.eventListeners[key]) this.eventListeners[key] = []
        this.eventListeners[key].push(listener[key]);
    }
}

// Project State Class
// =============================================================================
class GameState extends State<any> {
    private static instance: GameState
    private min: number | null = null
    private max: number = 0
    private current: number = 0
    private step: number | null = null
    private action: Events = Events.WELCOME
    private currentCardIdentifier: number = 0
    public numberOfMistakes: number = 0
    public playTime: number = 0
    public setOfNumbers: number[] = []
    public shuffledSetOfNumbers: number[] = []
    public currentIndex: number = 0
    public level: number = 0
    public finalLevel: number = 2
    public totalPlayTime: number = 0

    private constructor() {
        super()
    }

    public updateNumberOfMistakes() {
        this.numberOfMistakes++
        if(this.numberOfMistakes > 2) {
            this.triggerEvent(Events.FAILURE)
        }
    }

    public setState(stateObj: any) {
        const {
            numberOfMistakes,
            playTime,
            setOfNumbers,
            shuffledSetOfNumbers,
            level,
            totalPlayTime,
        } = stateObj
        if(numberOfMistakes != null) this.numberOfMistakes = numberOfMistakes
        if(playTime != null) {
            this.playTime = playTime
        }
        if(setOfNumbers != null) this.setOfNumbers = setOfNumbers
        if(shuffledSetOfNumbers != null) this.shuffledSetOfNumbers = shuffledSetOfNumbers
        if(level != null) this.level = level
        if(totalPlayTime != null) this.totalPlayTime = totalPlayTime
    }

    public get currentValue() {
        return this.current
    }

    public set currentValue(current) {
        this.current = current
    }

    public get minValue() {
        return this.min
    }

    public set minValue(min) {
        this.min = min
    }

    public get maxValue() {
        return this.max
    }

    public set maxValue(max) {
        this.max = max
    }

    public get stepValue() {
        return this.step
    }

    public set stepValue(step) {
        this.step = step
    }

    private get isGameFinished(): Boolean {
        if (this.currentValue == null || this.maxValue == null) return false
        return this.currentValue > this.maxValue
    }

    static getInstance() {
        return this.instance
            ? this.instance
            : new GameState()
    }

    public triggerEvent(e: Events) {
        if (e === Events.CLOSE_CARD) {
            this.run(e, this.currentValue)
            return
        }
        if (e === Events.MISTAKE) {
            this.run(e, this.currentCardIdentifier)
            return
        }
        this.run(e)
    }

    checkCurrentValue(cardIdentifier: number) {
        const isMatching = this.currentValue === cardIdentifier
        const userWinRound = this.currentValue === this.maxValue
        const userWinGame = this.level === (this.finalLevel - 1)

        this.currentCardIdentifier = cardIdentifier

        if (userWinRound && userWinGame) {
            this.triggerEvent(Events.WIN)
        }
        else if (userWinRound) {
            this.triggerEvent(Events.SUCCESS)
        }

        if (isMatching) {
            this.triggerEvent(Events.CLOSE_CARD)
            this.currentIndex++
            this.currentValue = this.setOfNumbers[this.currentIndex]
            return
        }

        this.triggerEvent(Events.MISTAKE)
    }
}

export const gameState = GameState.getInstance()
