export enum Events {
    WELCOME = 'WELCOME',
    START = 'START',
    SUCCESS = 'SUCCESS',
    FAILURE = 'FAILURE',
    TIMEUP = 'TIMEUP',
    MISTAKE = 'MISTAKE',
    CLOSE_CARD = 'CLOSE_CARD',
    RESTART = 'RESTART',
    START_NEW_LEVEL = 'START_NEW_LEVEL',
    WIN = 'WIN',
    SHOW_RANKING = 'SHOW_RANKING',
}

export type Listener = (i?: number) => void

export interface ListenerObject {
    [index: string]: Listener
}

export interface ListenerCollection {
    [index: string]: Listener[];
}

export interface ICard {
    identifier: number
    size?: number
}

export interface StoriesListeners {
    [index: string]: Listener[];
}

export interface IHTMLElement {
    tag?: string,
    className: string
}

