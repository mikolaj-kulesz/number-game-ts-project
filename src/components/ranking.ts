import {gameState} from "../state/game-state";
import {Events} from "../models/interfaces";

export class Ranking {

    readonly container: HTMLDivElement
    private wrapper: HTMLDivElement

    constructor() {
        this.wrapper = this.getWrapper()
        this.container = this.create()
        this.bindEventListeners()
        this.registerListeners()
        this.append()
        this.show().then()

    }

    private registerListeners(): void {
        gameState.addListener({
            [Events.SHOW_RANKING]:  () => {
                 this.show().then()
            }
        })
    }

    public async show() {
        const rankingItems = await this.getRankingItems()
        if(!rankingItems) return
        const list = this.getList()
        list.innerHTML = ``
        rankingItems.forEach(item => list.append(item))
        this.container.classList.add('on')
    }

    public hide() {
        this.container.classList.remove('on')
    }

    private getList() {
        return this.container.querySelector('.ranking-list')! as HTMLDivElement
    }

    private create(): HTMLDivElement {
        const rankingWrapper = document.createElement("div");
        const rankingContainer = document.createElement("div");
        const headline = document.createElement("h1");
        const list = document.createElement("ul");
        const closeButton = document.createElement("div");
        rankingWrapper.classList.add('ranking-wrapper')
        rankingContainer.classList.add('ranking-container')
        headline.classList.add('ranking-headline')
        list.classList.add('ranking-list')
        closeButton.classList.add('ranking-close-button')
        headline.innerHTML = `Best scores:`
        rankingContainer.append(headline)
        rankingContainer.append(list)
        rankingContainer.append(closeButton)
        rankingWrapper.append(rankingContainer)

        return rankingWrapper
    }

    private async getRankingItems(): Promise<(HTMLLIElement)[]> {
        const url = `https://numbergame-575c4.firebaseio.com/scores.json?orderBy="score"&startAt=0&print=pretty`
        const ranking = await fetch(url).then(res => res.json())
        if (!ranking) return []
        return Object.keys(ranking)
            .sort((a: any, b: any) => ranking[a].score - ranking[b].score)
            .map((key) => {
                console.log()
                const {name, score} : {name: string, score: string} = ranking[key]
                if(!name || !score) return null
                const listItem = document.createElement("li");
                const nameItem = document.createElement("span");
                const scoreItem = document.createElement("span");
                nameItem.innerHTML = name
                scoreItem.innerHTML = `${score}s`
                listItem.classList.add('ranking-list-item')
                listItem.append(nameItem)
                listItem.append(scoreItem)
                return listItem
        }) as HTMLLIElement[]
    }

    private bindEventListeners(): void {
        const button = this.container.querySelector('.ranking-close-button')! as HTMLButtonElement
        if(!button) return
        button.addEventListener('click', this.hide.bind(this))
    }

    private getWrapper(): HTMLDivElement {
        return document.querySelector('#app')! as HTMLDivElement
    }

    private append() {
        this.wrapper.appendChild(this.container);
    }

    private wait(ms: number) {
        return new Promise(res => setTimeout(res, ms));
    }
}
