import {gameState} from '../state/game-state'
import {Events} from "../models/interfaces";

export class MistakeCounter {
    readonly element: HTMLDivElement
    private wrapper: HTMLDivElement
    private max: number = 3

    constructor() {
        this.wrapper = this.getWrapper()
        this.element = this.createElement()
        this.appendElement()
        this.registerEventListener()
    }

    private createElement(): HTMLDivElement {
        const mistakeCounter = document.createElement("div");
        const label = document.createElement("span");
        const mistakeNumber = document.createElement("strong");
        mistakeCounter.classList.add('mistake-counter')
        label.classList.add('level-counter-label')
        mistakeCounter.innerHTML = 'Mistakes: '
        mistakeNumber.classList.add('mistake-counter-number')
        mistakeCounter.append(label)
        mistakeCounter.append(mistakeNumber)
        return mistakeCounter
    }

    private registerEventListener(): void {
        gameState.addListener({
            [Events.MISTAKE]: () => {
                this.updateNumberOfMistakes()
            },
            [Events.RESTART]: () => {
                this.restart()
            },
            [Events.START]: () => {
                this.restart()
            },
            [Events.START_NEW_LEVEL]: () => {
                this.restart()
            }
        })
    }

    private getWrapper(): HTMLDivElement {
        return document.querySelector('.game-section')! as HTMLDivElement
    }

    private appendElement() {
        this.restart()
        this.wrapper.appendChild(this.element);
    }

    private restart() {
        const numberElement: HTMLDivElement = this.getNumberElement()
        numberElement.innerHTML = `0 / ${this.max}`
    }

    private updateNumberOfMistakes() {
        const numberElement: HTMLDivElement = this.getNumberElement()
        numberElement.innerHTML = `${gameState.numberOfMistakes} / ${this.max}`
    }

    private getNumberElement(): HTMLDivElement {
        return this.element.querySelector('.mistake-counter-number')! as HTMLDivElement
    }
}
