import {gameState} from '../state/game-state'
import {Events} from "../models/interfaces";
import {BaseProjectComponent} from './base-project-component';

export class ProgressBar extends BaseProjectComponent {
    readonly element: HTMLElement
    readonly wrapper: HTMLElement
    private currentStep: number = 0

    constructor() {
        super()
        this.wrapperSelector = '.game-section';
        this.wrapper = this.getWrapper()
        this.element = this.create()
        this.revealProgressBar()
        this.append()
        this.registerListener()
    }

    public create(): HTMLElement {
        const progressBar = this.createElement({
            className: 'progress-bar'
        });
        const frontLayer = this.createElement({
            className: 'progress-bar-front-layer'
        });
        progressBar.append(frontLayer)
        return progressBar
    }

    private registerListener(): void {
        gameState.addListener({
            [Events.CLOSE_CARD]: () => {
                this.moveOneStepFurther()
            }
        })

        gameState.addListener({
            [Events.START]:  () => {
                this.restart()
            }
        })
    }

    private revealProgressBar(): void {
        this.element.classList.add('active')
    }

    private restart() {
        this.currentStep = 0
        this.resetFrontLayer()
    }

    private moveOneStepFurther() {
        const {stepValue} = gameState
        const frontLayer: HTMLDivElement = this.getFrontLayer()
        this.currentStep = stepValue ? this.currentStep + stepValue : this.currentStep
        frontLayer.style.width = `${this.currentStep * 100}%`
    }

    private resetFrontLayer(): void {
        const frontLayer: HTMLDivElement = this.getFrontLayer()
        frontLayer.style.width = '0'
    }

    private getFrontLayer(): HTMLDivElement {
        return this.element.querySelector('.progress-bar-front-layer')! as HTMLDivElement
    }
}
