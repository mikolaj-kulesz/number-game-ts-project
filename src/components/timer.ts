import {gameState} from '../state/game-state'
import {Events} from "../models/interfaces";

export class Timer {
    readonly timeProgressBar: HTMLDivElement
    readonly timer: HTMLDivElement
    private initialCount: number = 30
    private count: number = 0
    private counter: any = null
    private wrapper: HTMLDivElement
    private currentStep: number = 0
    private newDate: Date = new Date()
    private endDate: Date = new Date()

    constructor() {
        this.wrapper = this.getWrapper()
        this.timeProgressBar = this.createProgressBar()
        this.timer = this.createTimer()
        this.appendProgressBar()
        this.registerListener()
    }

    private createProgressBar(): HTMLDivElement {
        const progressBar = document.createElement("div");
        const frontLayer = document.createElement("div");
        progressBar.classList.add('time-progress-bar')
        frontLayer.classList.add('timer-progress-bar-front-layer')
        progressBar.append(frontLayer)
        return progressBar
    }

    private registerListener(): void {
        gameState.addListener({
            [Events.START]: () => {
                this.setTimer()
            },
            [Events.SUCCESS]: () => {
                this.successHandler()
            },
            [Events.WIN]: () => {
                this.successHandler()
            },
            [Events.FAILURE]: () => {
                this.clearTimer()
            },
            [Events.TIMEUP]: () => {
                this.clearTimer()
            }
        })
    }

    private createTimer(): HTMLDivElement {
        const timer = document.createElement("div");
        timer.classList.add('timer')
        return timer
    }

    private successHandler() {
        this.endDate = new Date()
        this.savePlayTime()
        this.clearTimer()
    }

    private savePlayTime() {
        const {totalPlayTime} = gameState
        const playTime = +(((this.endDate.getTime() - this.newDate.getTime()) / 1000).toFixed(2))
        gameState.setState({
            playTime,
            totalPlayTime: +((totalPlayTime + playTime).toFixed(2))
        })
    }

    private setTimer() {
        this.newDate = new Date()
        if (this.counter) this.clearTimer()
        this.count = this.initialCount
        this.printTime(this.count)
        this.resetFrontLayer()
        this.counter = setInterval(this.checkTime.bind(this), 1000); //1000 will  run it every 1 second
    }

    private checkTime() {
        this.count = this.count - 1;
        this.printTime(this.count)
        if (this.count <= 0) {
            this.clearTimer();
            gameState.triggerEvent(Events.TIMEUP)
            return;
        }
        this.printTime(this.count)
        this.updateProgressBar(this.count - 1)
    }

    private clearTimer() {
        clearInterval(this.counter);
    }

    private printTime(secondsLeft: number) {
        const {totalPlayTime} = gameState
        const className = this.getColorClass(secondsLeft)
        this.timer.innerHTML = `<span class="${className}">${secondsLeft.toString()}<small>s</small> / <strong>Total: ${totalPlayTime}</strong><small>s</small></span>`
    }

    private getColorClass(secondsLeft: number): string {
        let className: string = ''
        if (secondsLeft < 8) className = 'orange'
        if (secondsLeft < 3) className = 'red'
        return className
    }

    private getWrapper(): HTMLDivElement {
        return document.querySelector('.game-section')! as HTMLDivElement
    }

    private appendProgressBar() {
        this.wrapper.appendChild(this.timeProgressBar);
        this.wrapper.appendChild(this.timer);
    }

    private restart() {
        this.currentStep = 0
        this.resetFrontLayer()
    }

    private updateProgressBar(secondsLeft: number) {
        const currentWidth = secondsLeft / (this.initialCount - 1)
        const frontLayer: HTMLDivElement = this.getFrontLayer()
        const color = this.getColorClass(secondsLeft)
        frontLayer.style.width = `${currentWidth * 100}%`
        frontLayer.style.backgroundColor = color
    }

    private resetFrontLayer(): void {
        const frontLayer: HTMLDivElement = this.getFrontLayer()
        frontLayer.style.width = '100%'
        frontLayer.style.backgroundColor = ''
    }

    private getFrontLayer(): HTMLDivElement {
        return this.timeProgressBar.querySelector('.timer-progress-bar-front-layer')! as HTMLDivElement
    }
}
