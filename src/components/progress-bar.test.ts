import { ProgressBar } from './progress-bar'
import {gameState} from '../state/game-state'
import {Events} from "../models/interfaces";

test("ProgressBar Class Test", async () =>{
    document.body.innerHTML =
        `<div class="game-section">
        </div>`;
    const progressBar = new ProgressBar();
    expect(progressBar instanceof ProgressBar).toBe(true)

    gameState.stepValue = 1 / 5
    gameState.triggerEvent(Events.CLOSE_CARD);
    console.log('progressBar[\'currentStep\']', progressBar['currentStep'])
    expect(progressBar['currentStep']).toBe(0.2)
    gameState.triggerEvent(Events.CLOSE_CARD);
    expect(progressBar['currentStep']).toBe(0.4)

});
