// import Cmp from "./base-project-component";
// import {Project} from "../state/project";
// import {Draggable} from "../models/interfaces";
// import {AutoBind} from "../utils/auto-bind";
//
// export class ProjectItem extends Cmp<HTMLUListElement, HTMLLIElement> implements Draggable {
//     constructor(private project: Project) {
//         super(`${project.type}-projects-list`, 'single-project', project.id, 'beforeend');
//         this.renderItem();
//         this.bindListeners();
//     }
//
//     get personsProperWording() {
//         const {people} = this.project
//         switch (true) {
//             case people <= 0:
//                 return `nobody`
//             case people === 1:
//                 return `${people} person`
//             case people > 1:
//                 return `${people} persons`
//             default:
//                 return `nobody`
//         }
//     }
//
//     bindListeners(): void {
//         this.element.addEventListener('dragstart', this.dragStartHandler)
//         this.element.addEventListener('dragend', this.dragEndHandler)
//     }
//
//     renderItem() {
//         const {
//             title,
//             description,
//             people,
//         } = this.project
//         this.createElement('h2', title)
//         this.createElement('p', description)
//         this.createElement('small', `People: ${this.personsProperWording}.`)
//     }
//
//     @AutoBind
//     dragStartHandler(event: DragEvent): void {
//         event.dataTransfer!.setData('text/plain', this.project.id)
//         event.dataTransfer!.effectAllowed = 'move'
//     }
//
//     @AutoBind
//     dragEndHandler(event: DragEvent): void {
//         // console.log("drag end")
//     }
//
//     private createElement(tagName: string, content: string | number) {
//         const element = document.createElement(tagName);
//         element.textContent = content.toString()
//         this.element.insertAdjacentElement('beforeend', element);
//     }
// }
