import {gameState} from "../state/game-state";
import {Events} from "../models/interfaces";

export class Modal {

    readonly container: HTMLDivElement
    private wrapper: HTMLDivElement
    private messages: Map<Events, string>
    private buttonMessages: Map<Events, string>
    readonly customEvent: Events

    constructor(e: Events) {
        this.customEvent = e
        this.messages = this.getMessages()
        this.buttonMessages = this.getButtonMessages()
        this.wrapper = this.getWrapper()
        this.container = this.create()
        this.bindEventListeners()
        this.registerListeners()
        this.append()
    }

    private getMessages(): Map<Events, string> {
        const m = new Map()
        m.set(Events.WELCOME, 'Do you want to play?')
        m.set(Events.SUCCESS, 'Congratulations! You Win!')
        m.set(Events.FAILURE, 'Sorry, you loose :(')
        m.set(Events.TIMEUP, 'Sorry, your time is up, you loose. :(')
        m.set(Events.WIN, 'Congratulations. You win the whole game. You are the real counter master!')
        return m
    }

    private getButtonMessages(): Map<Events, string> {
        const m = new Map()
        m.set(Events.WELCOME, `Let's play!`)
        m.set(Events.SUCCESS, `Go to next round`)
        m.set(Events.FAILURE, `Start over again`)
        m.set(Events.TIMEUP, `Start over again`)
        m.set(Events.WIN, `Do you want to play again?`)
        return m
    }

    private registerListeners(): void {
        gameState.addListener({
            [this.customEvent]:  () => {
                 this.showModal()
            }
        })
    }

    public showModal() {
        this.addAdditionalInfo()
        this.container.classList.add('on')
    }

    private addAdditionalInfo() {
        if (this.customEvent === Events.SUCCESS) this.addAdditionalSuccessInfo()
        if (this.customEvent === Events.WIN) this.addAdditionalWinInfo()
        if (this.customEvent === Events.FAILURE) this.addAdditionalFailureInfo()
    }

    private addAdditionalSuccessInfo() {
        const placeholder = this.getPlaceholder()
        placeholder.innerHTML = `Your time is <strong>${gameState.playTime}</strong> seconds!`
    }

    private addAdditionalWinInfo() {
        const placeholder = this.getPlaceholder()
        // placeholder.innerHTML = `Completion time of last round is <strong>${gameState.playTime}</strong> seconds. Completion time of whole game is <strong>${gameState.totalPlayTime} seconds. Would you like to save your score?</strong>`
        const button = document.createElement('button')
        const input  = document.createElement('input')
        const form = document.createElement('form')
        button.innerHTML = "Add your score!"
        input.type = "text"
        input.placeholder = "Your nickname..."
        form.append(input)
        form.append(button)
        placeholder.append(form)
        button.addEventListener('click', this.sendScore.bind(this))
    }

    private async sendScore(e: Event) {
        e.preventDefault()
        console.log('sendScore')
        const url = `https://numbergame-575c4.firebaseio.com/scores.json`
        const name = this.getWrapper().querySelector('input')!.value
        const data = {
            name,
            score: gameState.totalPlayTime
        }
        await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })

    }

    private addAdditionalFailureInfo() {
        const placeholder = this.getPlaceholder()
        placeholder.innerHTML = `<strong>${gameState.numberOfMistakes}</strong> mistakes!`
    }

    private getPlaceholder() {
        return this.container.querySelector('.modal-placeholder')! as HTMLDivElement
    }

    public hideModal() {
        this.container.classList.remove('on')

        switch(this.customEvent){
            case Events.WELCOME: gameState.triggerEvent(Events.START); break
            case Events.SUCCESS: gameState.triggerEvent(Events.START_NEW_LEVEL); break
            case Events.FAILURE: gameState.triggerEvent(Events.RESTART); break
            case Events.TIMEUP: gameState.triggerEvent(Events.RESTART); break
            case Events.WIN: gameState.triggerEvent(Events.SHOW_RANKING); break
        }
    }

    private create(): HTMLDivElement {
        const modal = document.createElement("div");
        const message = document.createElement("h1");
        const button = document.createElement("button");
        const modalContainer = document.createElement("div");
        const placeholder = document.createElement("div");
        modal.classList.add('modal')
        message.classList.add('modal-message')
        button.classList.add('modal-button')
        modalContainer.classList.add('modal-container')
        placeholder.classList.add('modal-placeholder')
        message.innerHTML = `${this.messages.get(this.customEvent)}`
        button.innerHTML = `${this.buttonMessages.get(this.customEvent)}`
        modalContainer.append(message)
        modalContainer.append(placeholder)
        modalContainer.append(button)
        modal.append(modalContainer)
        return modal
    }

    private bindEventListeners(): void {
        const button = this.container.querySelector('.modal-button')! as HTMLButtonElement
        button.addEventListener('click', this.hideModal.bind(this))
    }

    private getWrapper(): HTMLDivElement {
        return document.querySelector('#app')! as HTMLDivElement
    }

    private append() {
        this.wrapper.appendChild(this.container);
    }

    private wait(ms: number) {
        return new Promise(res => setTimeout(res, ms));
    }
}
