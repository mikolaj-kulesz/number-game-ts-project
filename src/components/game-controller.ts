import {Card} from './card'
import {gameState} from '../state/game-state'
import {Events} from "../models/interfaces";

export class GameController {

    readonly container: HTMLElement | null
    private wrapper: HTMLDivElement

    constructor() {
        this.container = this.create()
        this.wrapper = this.getWrapper()
        this.append()
        this.registerListeners()
    }

    private registerListeners(): void {
        gameState.addListener({
            [Events.START]: () => {
                this.startGame()
            },
            [Events.MISTAKE]: () => {
                gameState.updateNumberOfMistakes()
            },
        })
    }

    private startGame() {
        this.clearCanvas()
        gameState.setState({
            numberOfMistakes: 0,
            playTime: 0,
        })
        this.initializeCards()
    }

    private clearCanvas() {
        const cardWrapper = this.wrapper.querySelector('.card-wrapper')! as HTMLDivElement
        cardWrapper.innerHTML = ''
    }

    private create() {
        const section = document.createElement("section");
        const container = document.createElement("div");
        const headline = document.createElement("h2");
        const cardWrapper = document.createElement("div");
        section.classList.add('game-section')
        headline.classList.add('card-headline')
        headline.innerHTML = `Click numbers in ascending order:`
        cardWrapper.classList.add('card-wrapper')
        container.append(headline)
        container.append(cardWrapper)
        section.append(container)
        return section
    }

    private initializeCards() {
        gameState.shuffledSetOfNumbers.forEach(number => {
            new Card({
                identifier: number
            })
        })
    }

    private getWrapper(): HTMLDivElement {
        return document.querySelector('#app')! as HTMLDivElement
    }

    private endGame() {

    }

    private cleanCanvas() {

    }

    private restartGame() {
        this.cleanCanvas()
        this.startGame();
    }

    private checkResults() {

    }

    private append(): void {
        if (this.container) this.wrapper.appendChild(this.container);
    }
}
