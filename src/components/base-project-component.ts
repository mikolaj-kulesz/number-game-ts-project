import {IHTMLElement} from "../models/interfaces";

export class BaseProjectComponent {
    readonly element: HTMLElement
    readonly wrapper: HTMLElement
    public wrapperSelector: string = '.some-selector';

    constructor() {
        this.wrapper = this.getWrapper()
        this.element = this.create()
    }

    public create(): HTMLElement {
        return document.createElement("div");
    }

    public createElement({tag = 'div', className} : IHTMLElement): HTMLElement {
        const element = document.createElement(tag);
        element.classList.add(className)
        return element
    }

    public getWrapper(): HTMLDivElement {
        return document.querySelector(this.wrapperSelector)! as HTMLDivElement
    }

    public append() {
        this.wrapper.appendChild(this.element);
    }
}
