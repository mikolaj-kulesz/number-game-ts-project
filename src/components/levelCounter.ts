import {gameState} from '../state/game-state'
import {Events} from "../models/interfaces";

export class LevelCounter {
    readonly element: HTMLDivElement
    private wrapper: HTMLDivElement

    constructor() {
        this.wrapper = this.getWrapper()
        this.element = this.createElement()
        this.appendElement()
        this.registerEventListener()
    }

    private createElement(): HTMLDivElement {
        const levelCounter = document.createElement("div");
        const label = document.createElement("span");
        const levelNumber = document.createElement("strong");
        levelCounter.classList.add('level-counter')
        label.classList.add('level-counter-label')
        label.innerHTML = 'Level: '
        levelNumber.classList.add('level-counter-number')
        levelCounter.append(label)
        levelCounter.append(levelNumber)
        return levelCounter
    }

    private registerEventListener(): void {
        gameState.addListener({
            [Events.SUCCESS]: () => {
                this.moveOneStepFurther()
            },
            [Events.RESTART]: () => {
                this.restart()
            }
        })
    }

    private getWrapper(): HTMLDivElement {
        return document.querySelector('.game-section')! as HTMLDivElement
    }

    private appendElement() {
        this.resetNumberElement()
        this.wrapper.appendChild(this.element);
    }

    private restart() {
        gameState.setState({
            level: 0,
        })
        this.resetNumberElement()
    }

    private moveOneStepFurther() {
        const numberElement: HTMLDivElement = this.getNumberElement()
        gameState.setState({
            level: gameState.level + 1,
        })
        numberElement.innerHTML = `${gameState.level} / 10`
    }

    private resetNumberElement(): void {
        const numberElement: HTMLDivElement = this.getNumberElement()
        numberElement.innerHTML = `${gameState.level} / 10`
    }

    private getNumberElement(): HTMLDivElement {
        return this.element.querySelector('.level-counter-number')! as HTMLDivElement
    }
}
