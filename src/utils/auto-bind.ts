export function AutoBind(target: any, propName: string, descriptor: PropertyDescriptor): PropertyDescriptor {
    const originalMethod = descriptor.value;
    return {
        get() {
            return originalMethod.bind(this);
        }
    }
}
