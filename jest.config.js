const {defaults} = require('jest-config');
module.exports = {
  preset: 'ts-jest',
  // testEnvironment: 'node',
  moduleFileExtensions: [...defaults.moduleFileExtensions, 'ts', 'tsx'],
  collectCoverage: true,
  // ...
};

// "jest": {
//   "transform": {
//     ".(ts|tsx)": "<rootDir>/node_modules/ts-jest/preprocessor.js"
//   },
//   "testRegex": "(/__tests__/.*|\\.(test|spec))\\.(ts|tsx|js)$",
//       "moduleFileExtensions": ["ts", "tsx", "js"],
//       "testResultsProcessor": "jest-teamcity-reporter"
// },
